
    <?php //echo $this->element("sidebar"); ?>
    <div class="patients index  ">
        <h2><?php echo __('Patients'); ?></h2>
        <table class="table" cellpadding="0" cellspacing="0">
        <thead>
        <tr>
                <th><?php echo $this->Paginator->sort('name'); ?></th>
                <th><?php echo $this->Paginator->sort('surname'); ?></th>
                <th><?php echo $this->Paginator->sort('phone'); ?></th>
                <th><?php echo $this->Paginator->sort('token'); ?></th>
                <th><?php echo $this->Paginator->sort('sex'); ?></th>
                <th><?php echo $this->Paginator->sort('consent_push'); ?></th>
                <th class="actions"><?php echo __('Actions'); ?></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($patients as $patient): ?>
        <tr>
            <td><?php echo h($patient['Patient']['name']); ?>&nbsp;</td>
            <td><?php echo h($patient['Patient']['surname']); ?>&nbsp;</td>
            <td><?php echo h($patient['Patient']['phone']); ?>&nbsp;</td>
            <td><?php echo h($patient['Patient']['token']); ?>&nbsp;</td>
            <td><?php echo h($patient['Patient']['sex']); ?>&nbsp;</td>
            <td><?php echo ($patient['Patient']['consent_push'])? __("tak"):__("nie"); ?>&nbsp;</td>
            <td class="actions">
                <?php echo $this->Html->link(__('View'), array('action' => 'view', $patient['Patient']['id'])); ?>
                <?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $patient['Patient']['id'])); ?>
                <?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $patient['Patient']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $patient['Patient']['id']))); ?>
            </td>
        </tr>
    <?php endforeach; ?>
        </tbody>
        </table>
        <?php if($patientsHasPages){ ?>
        <nav>
            <ul class="pagination">
                <?php
                echo $this->Paginator->prev('< ' . __('<<'), array(), null, array('class' => 'prev disabled'));
                echo $this->Paginator->numbers(array('separator' => ''));
                echo $this->Paginator->next(__('>>') . ' >', array(), null, array('class' => 'next disabled'));
                ?>
            </ul>
        </nav>
        <?php } ?>
    </div>