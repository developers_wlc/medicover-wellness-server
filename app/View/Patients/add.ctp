<?php echo $this->Form->create('Patient', array("class" => "patient-form")); ?>
    <fieldset>
        <legend><?php echo __('Add Patient'); ?></legend>
    <?php

        $bClasses = array("div"=> array("class"=> "form-group"), "class"=>"form-control");

        echo $this->Form->input('Patient.name', array("div"=> array("class"=> "form-group"), "class"=>"form-control"));
        echo $this->Form->input('Patient.surname', array("div"=> array("class"=> "form-group"), "class"=>"form-control"));
        echo $this->Form->input('Patient.phone', array("div"=> array("class"=> "form-group"), "class"=>"form-control"));
        echo $this->Form->input('Patient.sex', array(
            "div"=> array("class"=> "form-group"),
            "class"=>"form-control",
            'options' => array(
                'male' => __("male", true),
                "female" => __("female", true)
            )
        ));?>
    </fieldset>
<div class="boundary">
    <fieldset>
        <legend><?php echo __('Pulse'); ?></legend>
        <?php echo $this->Form->input('Boundaries.0.pulse_min', $bClasses);?>
        <?php echo $this->Form->input('Boundaries.0.pulse_current' ,$bClasses);?>
        <?php echo $this->Form->input('Boundaries.0.pulse_max', $bClasses);?>
    </fieldset>
    <fieldset>
        <legend><?php echo __('Pressure Systolic'); ?></legend>
        <?php echo $this->Form->input('Boundaries.0.pressure_systolic_min', $bClasses);?>
        <?php echo $this->Form->input('Boundaries.0.pressure_systolic_current', $bClasses);?>
        <?php echo $this->Form->input('Boundaries.0.pressure_systolic_max', $bClasses);?>
    </fieldset>
    <fieldset>
        <legend><?php echo __('Pressure Diastolic'); ?></legend>
        <?php echo $this->Form->input('Boundaries.0.pressure_diastolic_min', $bClasses);?>
        <?php echo $this->Form->input('Boundaries.0.pressure_diastolic_current', $bClasses);?>
        <?php echo $this->Form->input('Boundaries.0.pressure_diastolic_max', $bClasses);?>
    </fieldset>
    <fieldset>
        <legend><?php echo __('Weight'); ?></legend>
        <?php echo $this->Form->input('Boundaries.0.weight_min', $bClasses);?>
        <?php echo $this->Form->input('Boundaries.0.weight_current', $bClasses);?>
        <?php echo $this->Form->input('Boundaries.0.weight_max', $bClasses);?>
    </fieldset>
    <fieldset>
        <legend><?php echo __('Bmi'); ?></legend>
        <?php echo $this->Form->input('Boundaries.0.bmi_min', $bClasses);?>
        <?php echo $this->Form->input('Boundaries.0.bmi_current', $bClasses);?>
        <?php echo $this->Form->input('Boundaries.0.bmi_max', $bClasses);?>
    </fieldset>
    <fieldset>
        <legend><?php echo __('Muscle Mass'); ?></legend>
        <?php echo $this->Form->input('Boundaries.0.muscle_mass_min', $bClasses);?>
        <?php echo $this->Form->input('Boundaries.0.muscle_mass_current', $bClasses);?>
        <?php echo $this->Form->input('Boundaries.0.muscle_mass_max', $bClasses);?>
    </fieldset>
    <fieldset>
        <legend><?php echo __('Fat'); ?></legend>
        <?php echo $this->Form->input('Boundaries.0.fat_min', $bClasses);?>
        <?php echo $this->Form->input('Boundaries.0.fat_current', $bClasses);?>
        <?php echo $this->Form->input('Boundaries.0.fat_max', $bClasses);?>
    </fieldset>
    <fieldset>
        <legend><?php echo __('Steps'); ?></legend>
        <?php echo $this->Form->input('Boundaries.0.step_min', $bClasses);?>
        <?php echo $this->Form->input('Boundaries.0.step_max', $bClasses);?>
    </fieldset>
    <fieldset>
        <legend><?php echo __('calories'); ?></legend>
        <?php echo $this->Form->input('Boundaries.0.calorie_min', $bClasses);?>
        <?php echo $this->Form->input('Boundaries.0.calorie_max', $bClasses);?>
    </fieldset>
    <fieldset>
        <legend><?php echo __('weather_temperature'); ?></legend>
        <?php echo $this->Form->input('Boundaries.0.weather_temperature_min', $bClasses);?>
        <?php echo $this->Form->input('Boundaries.0.weather_temperature_max', $bClasses);?>
    </fieldset>
    <fieldset>
        <legend><?php echo __('weather_humidity'); ?></legend>
        <?php echo $this->Form->input('Boundaries.0.weather_humidity_min', $bClasses);?>
        <?php echo $this->Form->input('Boundaries.0.weather_humidity_max', $bClasses);?>
    </fieldset>
    <fieldset>
        <legend><?php echo __('distance'); ?></legend>
        <?php echo $this->Form->input('Boundaries.0.distance_min', $bClasses);?>
        <?php echo $this->Form->input('Boundaries.0.distance_max', $bClasses);?>
    </fieldset>
    <fieldset>
        <legend><?php echo __('weather_pressure'); ?></legend>
        <?php echo $this->Form->input('Boundaries.0.weather_pressure_min', $bClasses);?>
        <?php echo $this->Form->input('Boundaries.0.weather_pressure_max', $bClasses);?>
    </fieldset>
</div>
    <?php echo $this->Form->submit(
        __('Submit', true),
        array('class' => 'btn btn-default'));
    ?>
<?php echo $this->Form->end(); ?>