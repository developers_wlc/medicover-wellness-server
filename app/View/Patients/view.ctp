<?php
$scriptString = "function drawChart() {";

foreach($patient["FormattedBoundary"] as $key => $boundary) {
    if(!isset($boundary["current"])){
        continue;
    }

    $middle = ($boundary['max'] - $boundary['min']) / 2;

    $min = $boundary['min'] - (abs($middle*0.5));
    $max = $boundary['max'] + (abs($middle*0.5));

    $scriptString .= "
        var chart_".$key."_data = google.visualization.arrayToDataTable([
        ['Label', 'Value'],
        ['', " . $boundary["current"] . "], ]);

        var options".$key." = {
            min: ".$min.",
            max: ".$max.",
            width: 200,
            height: 200,
            redFrom: ".$min.",
            redTo: ".$boundary['min'].",
            yellowFrom: ".$boundary['max'].",
            yellowTo: ".$max.",
            //greenFrom: 0,
            //greenTo: 25,
            minorTicks: 5,
            yellowColor: 'red',
            //greenColor: 'white',
            redColor: 'red'
        };
        var chart".$key." = new google.visualization.Gauge(document.getElementById('".$key."_div'));

        chart".$key.".draw(chart_".$key."_data, options".$key.");
        ";
}
$scriptString .= "}";
?>
<?php echo $this->Html->scriptBlock($scriptString, array('inline' => false)); ?>
<?php echo $this->Html->script('gauge', array('block' => 'script')); ?>
<?php echo $this->element("breadcrumbs", array("crumbs"=>array(
    array(
        "title"=> __("patients-list"),
        "url"=>array ("controller"=> "patients", "action"=>"index"),
        "options"=>array()),
    array(
        "title"=> $patient["Patient"]["label"],
        "url"=>array ("controller"=> "patients", "action"=>"view", $patient["Patient"]["id"]),
        "options"=>array())
))); ?>
<div class="patient">
    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#panicModal" data-whatever="@getbootstrap">Open modal for @getbootstrap</button>
    <div class="section info">
        <div class="clearfix">
            <h2 class="pull-left"><?php echo h($patient['Patient']['label']); ?></h2>
            <?php echo $this->Html->link(
                __("edit"),
                array('controller' => 'patients',
                    'action' => 'edit',
                    $patient['Patient']['id']
                ),array("class"=>"btn btn-default pull-right")); ?>
        </div>
        <div class="row">
            <div class="col-xs-6 col-md-6 col-lg-6">
                <dl class="dl-horizontal">
                    <dt><?php echo __('Token'); ?></dt>
                    <dd><?php echo h($patient['Patient']['token']); ?></dd>
                    <dt><?php echo __('Phone'); ?></dt>
                    <dd><?php echo h($patient['Patient']['phone']); ?></dd>
                </dl>
            </div>
            <div class="col-xs-6 col-md-6 col-lg-6">
                <dl class="dl-horizontal">
                    <dt><?php echo __('Sex'); ?></dt>
                    <dd><?php echo h($patient['Patient']['sex']); ?></dd>
                    <dt><?php echo __('Consent Push'); ?></dt>
                    <dd><?php echo h($patient['Patient']['consent_push']); ?></dd>
                </dl>
            </div>
        </div>
    </div>
    <div class="section">
        <ul class="nav nav-tabs nav-justified">
            <li role="presentation" class="active">
                <?php echo $this->Html->link(
                    __("boundary"),
                    array('controller' => 'patients',
                        'action' => 'view',
                        $patient['Patient']['id']
                    ),array("class"=>"")); ?>
            </li>
            <li role="presentation">
                <?php echo $this->Html->link(
                    __("messages"),
                    array('controller' => 'messages',
                        'action' => 'user',
                        $patient['Patient']['id']
                    ),array("class"=>"")); ?>
            </li>
        </ul>
        <div class="boundary tab-content">
            <?php
            foreach($patient["FormattedBoundary"] as $key => $boundary) {
                if (!isset($boundary["current"])) {
                    continue;
                }
                echo "<div class='boundary-item'>";
                echo "<p>";
                echo __d("dynamic", $key);
                echo "</p>";
                echo "<div class='gauge' id='".$key."_div'></div>";
                echo "</div>";
            }
            ?>
            <?php
            foreach($patient["FormattedBoundary"] as $key => $boundary) {
                if (!isset($boundary["current"])) {
                    echo "<div class='boundary-item'>";
                    echo "<p>";
                    echo __d("dynamic", $key);
                    echo "</p>";
                    echo "<p>".__("min") ."&nbsp;". $boundary["min"] . "</p>";
                    echo "<p>".__("max") ."&nbsp;". $boundary["max"] . "</p>";
                    echo "</div>";
                }
            }
            ?>
        </div>
    </div>
</div>
<?php echo __("muscle_mass"); ?>
<div class="modal fade" id="panicModal" tabindex="-1" role="dialog" aria-labelledby="panicModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="panicModalLabel"><?php echo __("panic message"); ?></h4>
            </div>
            <div class="modal-body">
                <?php echo $this->Form->create(null, array('url' => array('controller' => 'patients', 'action' => 'push'))); ?>
                    <input type="hidden" name="patient_id" value="<?php echo $patient["Patient"]["id"] ?>">
                    <div class="form-group">
                        <label for="message-text" class="control-label">Message:</label>
                        <textarea name="body" class="form-control" id="PanicPushBody"></textarea>
                    </div>
                <?php echo $this->Form->end(); ?>
                <p id="PanicPushCharactersCounter"></p>
                <div class="alert alert-danger" role="alert">
                    <span class="sr-only">Error:</span>
                    <span class="body">oihoihoihoihoihoihoih</span>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary submitajax">Send message</button>
            </div>
        </div>
    </div>
</div>