<?php
header('x-aaa: application/json');
$frame = array(
    "error" => array(
        "error-title" => (isset($title))? $title:"Cake Error",
        "error-message" => $message,
        "error-options" => (isset($options))? $options: []
    )
);
echo json_encode($frame);
?>