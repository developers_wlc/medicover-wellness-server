<?php
/* /app/View/Helper/LinkHelper.php */
App::uses('AppHelper', 'View/Helper');

class UtcHelper extends AppHelper {
    public $helpers = array('Html');

    public function dateOut($date) {
        $class = "utcdate";
        if(empty($date) || $date == "" || $date == null || strlen($date) != 19){
            $class = "";
        }
        return $this->Html->tag('span', "", array('class' => $class, "date" => $date));
    }
}