<!DOCTYPE html>
<html lang="en">
    <head>
        <?php echo $this->Html->charset(); ?>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="../../favicon.ico">

        <title>
            <?php echo $this->fetch('title'); ?>
        </title>

        <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>


        <script type="text/javascript" src="https://www.google.com/jsapi"></script>

        <?php

        echo $this->Html->meta('icon');

        echo $this->Html->css('bootstrap.min');
        echo $this->Html->css('simple-sidebar');
        echo $this->Html->css('jquery.datetimepicker');
        //echo $this->Html->css('jquery.autocomplete');
        echo $this->Html->css('styles');

        //echo $this->fetch('meta');
        //echo $this->fetch('css');

        echo $this->Html->script('moment.min');
        echo $this->Html->script('jquery.datetimepicker');
        //echo $this->Html->script('jquery.autocomplete');
        echo $this->Html->script('script');

        echo $this->fetch('script');

        ?>
    </head>

    <body>
    <div id="wrapper">

        <!-- Sidebar -->
        <div id="sidebar-wrapper">
            <?php echo $this->element("sidebar") ?>
        </div>
        <!-- /#sidebar-wrapper -->

        <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="flash"><?php echo $this->Session->flash(); ?></div>
                <?php echo $this->fetch('content'); ?>
            </div>
        </div>
        <!-- /#page-content-wrapper -->
        <?php echo $this->element('sql_dump'); ?>
    </div>
    <!-- /#wrapper -->

        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <?php echo $this->Html->script('bootstrap.min'); ?>
        <script>
            $("#menu-toggle").click(function(e) {
                e.preventDefault();
                $("#wrapper").toggleClass("toggled");
            });
        </script>
    <?php echo $this->Js->writeBuffer(); ?>
    </body>
</html>