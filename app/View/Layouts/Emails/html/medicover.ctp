<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts.Email.html
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN">
<html>
<head>
</head>
    <body style="width:600px; margin:0 auto;">
    <center><?php echo $this->Html->image('mailing/logo_medi.png', array("style"=>"padding:35px;", 'fullBase'=>true, 'alt' => 'medicover')); ?></center>
    <div style="background-color:#2db7b0;">
        <?php echo $this->Html->image('mailing/szpital.png', array(
            "style"=>"display:inline-block; vertical-align:middle; border-right:2px solid white;",
            'fullBase'=>true,
            'alt' => 'szpital')); ?>
        <span style="display:inline-block; vertical-align:middle; font-size: 24px; color:white; padding-left: 20px;"><?php echo __("Witaj"); ?> <?php echo $message["doctor"]["label"] ?></span>
    </div>
    <div style="padding:36px 28px; ">
        <p style="margin-top:0; color: #88888f;"><?php echo __("Stan pacjenta"); ?> <strong><?php echo $message["patient"]["label"] ?></strong> <?php echo __("na dzień"); ?> <strong><?php echo $message["statusDateUtc"] ?></strong></p>
        <hr>
        <?php echo $this->fetch('content'); ?>
        <hr>
        <div style="margin-top:30px;">
            <center>
                <?php echo $this->Html->link(
                    $this->Html->image('mailing/btn_react.png', array('fullBase'=>true, 'alt' => 'react')),
                    array("controller"=>"messages", "action"=>"user", $message["patient"]["id"], "full_base"=>true),
                    array("escape"=>false)
                )?>
            </center>
        </div>
    </div>
    </body>
</html>