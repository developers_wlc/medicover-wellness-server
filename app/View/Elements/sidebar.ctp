<div class="sidebar">
<center>
    <a href="<?php echo $this->Html->url(array("controller" => "patients", "action"=>"index", "admin"=>false)); ?>">
        <img src="/img/logo_medicover.png" alt="medicover"/>
    </a>
    <? if($user){ ?>
        <div class="search input-group">
            <input id="autocomplete" type="text" class="form-control" />
        </div>

        <a href="<?php echo $this->Html->url(array("controller" => "patients", "action"=>"add", "admin"=>false)); ?>" class="add-patient btn btn-default btn-green">
            <center>
                <img class="plus" src="/img/logo_plus.png"/>
                <img src="/img/logo_patient.png"/>
                <span</span><?php echo __("dodaj pacjenta"); ?></span>
            </center>
        </a>
        <a href="<?php echo $this->Html->url(array("controller" => "messages", "action"=>"index", "admin"=>false)); ?>" class="btn btn-default btn-green">
            <?php echo __("Wiadomości"); ?>
        </a>
        <a href="<?php echo $this->Html->url(array("controller" => "patients", "action"=>"index", "admin"=>false)); ?>" class="btn btn-default btn-green">
            <?php echo __("Pacjenci"); ?>
        </a>
        <?php if($isAdmin){ ?>
            <a href="<?php echo $this->Html->url(array("controller" => "users", "action"=>"index", "admin"=>true)); ?>" class="btn btn-default btn-green">
                <?php echo __("Użytkownicy"); ?>
            </a>
        <? } ?>
        <div class="userinfo">
            <p><?php echo __("Zalogowany jako:"); ?></p>
            <div class="panel panel-default">
                <div class="panel-body">
                    <p><?= $user["role"]?></p>
                    <p><strong><?= $user["username"] ?></strong></p>
                    <p> <?php echo $this->Html->link(__("wyloguj"), array("controller"=> "users", "action"=>"logout", "admin"=>false)) ?></p>
                </div>
            </div>
        </div>
    <? } ?>
</center>
</div>