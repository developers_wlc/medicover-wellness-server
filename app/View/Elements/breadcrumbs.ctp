<?php if($crumbs){  ?>
<ol class="breadcrumb">
    <? foreach($crumbs as $crumb) { ?>
        <li><?php echo $this->Html->link(
                $crumb["title"],
                $crumb["url"],
                $crumb["options"]); ?></li>
    <?php } ?>
</ol>
<?php } ?>