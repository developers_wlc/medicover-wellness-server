<nav>
    <ul class="pagination">
        <?php
        echo $this->Paginator->prev('< ' . __('<<'), array(), null, array('class' => 'prev disabled'));
        echo $this->Paginator->numbers(array('separator' => ''));
        echo $this->Paginator->next(__('>>') . ' >', array(), null, array('class' => 'next disabled'));
        ?>
    </ul>
</nav>