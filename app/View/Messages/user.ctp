<?php echo $this->element("breadcrumbs", array("crumbs"=>array(
    array(
        "title"=> __("patients-list"),
        "url"=>array ("controller"=> "patients", "action"=>"index"),
        "options"=>array()),
    array(
        "title"=> $patient["Patient"]["name"],
        "url"=>array ("controller"=> "patients", "action"=>"view", $patient["Patient"]["id"]),
        "options"=>array()),
    array(
        "title"=> __("messages"),
        "url"=>array ("controller"=> "messages", "action"=>"user", $patient["Patient"]["id"]),
        "options"=>array())
))); ?>
<div class="patient">
    <div class="section info">
        <div class="clearfix">
            <h2 class="pull-left"><?php echo h($patient['Patient']['name']); ?> <?php echo h($patient['Patient']['surname']); ?></h2>
            <?php echo $this->Html->link(
                __("edit"),
                array('controller' => 'patients',
                    'action' => 'edit',
                    $patient['Patient']['id']
                ),array("class"=>"btn btn-default pull-right")); ?>
        </div>
        <div class="row">
            <div class="col-xs-6 col-md-6 col-lg-6">
                <dl class="dl-horizontal">
                    <dt><?php echo __('Token'); ?></dt>
                    <dd><?php echo h($patient['Patient']['token']); ?></dd>
                    <dt><?php echo __('Phone'); ?></dt>
                    <dd><?php echo h($patient['Patient']['phone']); ?></dd>
                </dl>
            </div>
            <div class="col-xs-6 col-md-6 col-lg-6">
                <dl class="dl-horizontal">
                    <dt><?php echo __('Sex'); ?></dt>
                    <dd><?php echo h($patient['Patient']['sex']); ?></dd>
                    <dt><?php echo __('Consent Push'); ?></dt>
                    <dd><?php echo h($patient['Patient']['consent_push']); ?></dd>
                </dl>
            </div>
        </div>
    </div>
    <div class="section">
        <ul class="nav nav-tabs nav-justified">
            <li role="presentation">
                <?php echo $this->Html->link(
                    __("boundary"),
                    array('controller' => 'patients',
                        'action' => 'view',
                        $patient['Patient']['id']
                    ),array("class"=>"")); ?>
            </li>
            <li role="presentation" class="active">
                <?php echo $this->Html->link(
                    __("messages"),
                    array('controller' => 'messages',
                        'action' => 'user',
                        $patient['Patient']['id']
                    ),array("class"=>"")); ?>
            </li>
        </ul>
        <div class="messages index tab-content">
        <? if($messageTypes){ ?>
            <ul class="nav nav-tabs">
                <? foreach($messageTypes as $messageId=> $messageName) {
                    //$messageType = $messageType["Messagetype"];
                    ?>
                    <li role="presentation" class="<? echo ($messageId == $selectedMessageType)? "active": ""?>">
                        <?php echo $this->Html->link(__("translate".$messageName), array(
                                'controller' => 'messages',
                                'action' => 'user',
                                $patient['Patient']['id'],
                                $messageId)
                        );?>
                    </li>
                <?}?>
            </ul>
        <?}?>
        <div class="new-message">
            <?php echo $this->Form->create('Message', array("class" => "")); ?>
            <fieldset>
                <legend><?php echo __('Add Message'); ?></legend>
                <?php

                $bClasses = array("div"=> array("class"=> "form-group"), "class"=>"form-control");

                echo $this->Form->hidden('patient_id', array("value" => $patient["Patient"]["id"]));
                echo $this->Form->hidden('messagetype', array("value" => $selectedMessageType));?>
                <div class="row">
                    <div class="col-md-5 col-lg-7">
                        <?php echo $this->Form->input('body', $bClasses); ?>
                    </div>
                    <div class="col-md-7 col-lg-5">
                        <?php echo $this->Form->input('date', array("label"=>false, 'type' => 'text', 'class'=>'datepicker'));?>
                    </div>
                </div>
            </fieldset>
            <p id="CharactersLeft">Zostało <span id="CharactersCounter"></span> znaków</p>
            <?php echo $this->Form->submit(
                __('Submit', true),
                array('class' => 'btn btn-default'));
            ?>
            <?php echo $this->Form->end(); ?>
        </div>
        <?php if($messages) { ?>
            <table class="table" cellpadding="0" cellspacing="0">
                <thead>
                <tr>
                    <th><?php echo $this->Paginator->sort('body'); ?></th>
                    <th><?php echo $this->Paginator->sort('sent'); ?></th>
                    <th><?php echo $this->Paginator->sort('date'); ?></th>
                    <th><?php echo $this->Paginator->sort('created'); ?></th>
                    <th class="actions"><?php echo __('Actions'); ?></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($messages as $message): ?>
                    <tr>
                        <td><?php echo h($message['Message']['body']); ?>&nbsp;</td>
                        <td><?php echo h($message['Message']['sent']); ?>&nbsp;</td>
                        <td><?php echo $this->Utc->dateOut($message['Message']['date']); ?>&nbsp;</td>
                        <td><?php echo $this->Utc->dateOut($message['Message']['created']); ?>&nbsp;</td>
                        <td class="actions">
                            <?php
                            if($message['Message']['sent']) {
                                echo $this->Form->postLink(__('Delete'), array("controller" => "messages", 'action' => 'delete', $message['Message']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $message['Message']['id'])));
                            }
                            ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
            <?php if($messageHasPages) echo $this->element("pagination"); ?>
        <?php } ?>
    </div>
    </div>
</div>