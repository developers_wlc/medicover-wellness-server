<div class="messages index">
	<h2><?php echo __('Messages'); ?></h2>
    <? if($messageTypes){ ?>
        <ul class="nav nav-tabs">
            <? foreach($messageTypes as $messageId=> $messageName) {
                //$messageType = $messageType["Messagetype"];
                ?>
                <li role="presentation" class="<? echo ($messageId == $selectedMessageType)? "active": ""?>">
                    <?php echo $this->Html->link(__("translate".$messageName), array(
                            'controller' => 'messages',
                            'action' => 'index',
                            $messageId)
                    );?>
                </li>
            <?}?>
        </ul>
    <?}?>
	<table class="table" cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('patient_id'); ?></th>
			<th><?php echo $this->Paginator->sort('body'); ?></th>
            <th><?php echo $this->Paginator->sort('date'); ?></th>
			<th><?php echo $this->Paginator->sort('sent'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($messages as $message): ?>
	<tr>
		<td>
			<?php echo $this->Html->link($message['Patient']['name'], array('controller' => 'patients', 'action' => 'view', $message['Patient']['id'])); ?>
		</td>
		<td><?php echo h($message['Message']['body']); ?>&nbsp;</td>
        <td><?php echo $this->Utc->dateOut($message['Message']['date']); ?>&nbsp;</td>
		<td><?php echo h($message['Message']['sent']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $message['Message']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $message['Message']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $message['Message']['id']))); ?>
		</td>
	</tr>
    <?php endforeach; ?>
	</tbody>
	</table>
    <?php if ($hasPages) echo $this->element("pagination"); ?>
</div>
