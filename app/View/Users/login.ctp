<?php echo $this->Session->flash('auth'); ?>
<?php echo $this->Form->create('User'); ?>
<fieldset>
    <legend>
        <?php echo __('Please enter your username and password'); ?>
    </legend>
    <?php
    echo $this->Form->input('username', array("div"=> array("class"=> "form-group"), "class"=>"form-control"));
    echo $this->Form->input('password', array("div"=> array("class"=> "form-group"), "class"=>"form-control"));
    ?>
</fieldset>
<?php echo $this->Form->submit(
    __('Login', true),
    array('class' => 'btn btn-default'));
?>
<?php echo $this->Form->end(); ?>