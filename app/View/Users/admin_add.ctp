<?php $bClasses = array("div"=> array("class"=> "form-group"), "class"=>"form-control"); ?>
<?php echo $this->Form->create('User', array("class"=> "")); ?>
<fieldset>
    <legend><?php echo __('Add User'); ?></legend>
    <?php
    echo $this->Form->input('username', $bClasses);
    echo $this->Form->input('name', $bClasses);
    echo $this->Form->input('surname', $bClasses);
    echo $this->Form->input('email', $bClasses);
    echo $this->Form->input('password', $bClasses);
    echo $this->Form->input('role', array_merge(array(
        'options' => array("doctor"=>__("Lekarz"), 'admin' => 'Admin')
    ), $bClasses));
    ?>
</fieldset>
<?php echo $this->Form->submit(
    __('Submit', true),
    array('class' => 'btn btn-default'));
?>
<?php echo $this->Form->end(); ?>