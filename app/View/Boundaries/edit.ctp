<div class="boundaries form">
<?php echo $this->Form->create('Boundary'); ?>
	<fieldset>
		<legend><?php echo __('Edit Boundary'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('patient_id');
		echo $this->Form->input('pulse_current');
		echo $this->Form->input('pulse_min');
		echo $this->Form->input('pulse_max');
		echo $this->Form->input('pressure_systolic_current');
		echo $this->Form->input('pressure_systolic_min');
		echo $this->Form->input('pressure_systolic_max');
		echo $this->Form->input('weight_current');
		echo $this->Form->input('weight_min');
		echo $this->Form->input('weight_max');
		echo $this->Form->input('bmi_current');
		echo $this->Form->input('bmi_min');
		echo $this->Form->input('bmi_max');
		echo $this->Form->input('muscle_mass_current');
		echo $this->Form->input('muscle_mass_min');
		echo $this->Form->input('muscle_mass_max');
		echo $this->Form->input('fat_current');
		echo $this->Form->input('fat_min');
		echo $this->Form->input('fat_max');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Boundary.id')), array(), __('Are you sure you want to delete # %s?', $this->Form->value('Boundary.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Boundaries'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Patients'), array('controller' => 'patients', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Patient'), array('controller' => 'patients', 'action' => 'add')); ?> </li>
	</ul>
</div>
