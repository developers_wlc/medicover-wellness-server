<div class="boundaries index">
	<h2><?php echo __('Boundaries'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('patient_id'); ?></th>
			<th><?php echo $this->Paginator->sort('pulse_current'); ?></th>
			<th><?php echo $this->Paginator->sort('pulse_min'); ?></th>
			<th><?php echo $this->Paginator->sort('pulse_max'); ?></th>
			<th><?php echo $this->Paginator->sort('pressure_systolic_current'); ?></th>
			<th><?php echo $this->Paginator->sort('pressure_systolic_min'); ?></th>
			<th><?php echo $this->Paginator->sort('pressure_systolic_max'); ?></th>
			<th><?php echo $this->Paginator->sort('weight_current'); ?></th>
			<th><?php echo $this->Paginator->sort('weight_min'); ?></th>
			<th><?php echo $this->Paginator->sort('weight_max'); ?></th>
			<th><?php echo $this->Paginator->sort('bmi_current'); ?></th>
			<th><?php echo $this->Paginator->sort('bmi_min'); ?></th>
			<th><?php echo $this->Paginator->sort('bmi_max'); ?></th>
			<th><?php echo $this->Paginator->sort('muscle_mass_current'); ?></th>
			<th><?php echo $this->Paginator->sort('muscle_mass_min'); ?></th>
			<th><?php echo $this->Paginator->sort('muscle_mass_max'); ?></th>
			<th><?php echo $this->Paginator->sort('fat_current'); ?></th>
			<th><?php echo $this->Paginator->sort('fat_min'); ?></th>
			<th><?php echo $this->Paginator->sort('fat_max'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('modified'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($boundaries as $boundary): ?>
	<tr>
		<td><?php echo h($boundary['Boundary']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($boundary['Patient']['name'], array('controller' => 'patients', 'action' => 'view', $boundary['Patient']['id'])); ?>
		</td>
		<td><?php echo h($boundary['Boundary']['pulse_current']); ?>&nbsp;</td>
		<td><?php echo h($boundary['Boundary']['pulse_min']); ?>&nbsp;</td>
		<td><?php echo h($boundary['Boundary']['pulse_max']); ?>&nbsp;</td>
		<td><?php echo h($boundary['Boundary']['pressure_systolic_current']); ?>&nbsp;</td>
		<td><?php echo h($boundary['Boundary']['pressure_systolic_min']); ?>&nbsp;</td>
		<td><?php echo h($boundary['Boundary']['pressure_systolic_max']); ?>&nbsp;</td>
		<td><?php echo h($boundary['Boundary']['weight_current']); ?>&nbsp;</td>
		<td><?php echo h($boundary['Boundary']['weight_min']); ?>&nbsp;</td>
		<td><?php echo h($boundary['Boundary']['weight_max']); ?>&nbsp;</td>
		<td><?php echo h($boundary['Boundary']['bmi_current']); ?>&nbsp;</td>
		<td><?php echo h($boundary['Boundary']['bmi_min']); ?>&nbsp;</td>
		<td><?php echo h($boundary['Boundary']['bmi_max']); ?>&nbsp;</td>
		<td><?php echo h($boundary['Boundary']['muscle_mass_current']); ?>&nbsp;</td>
		<td><?php echo h($boundary['Boundary']['muscle_mass_min']); ?>&nbsp;</td>
		<td><?php echo h($boundary['Boundary']['muscle_mass_max']); ?>&nbsp;</td>
		<td><?php echo h($boundary['Boundary']['fat_current']); ?>&nbsp;</td>
		<td><?php echo h($boundary['Boundary']['fat_min']); ?>&nbsp;</td>
		<td><?php echo h($boundary['Boundary']['fat_max']); ?>&nbsp;</td>
		<td><?php echo h($boundary['Boundary']['created']); ?>&nbsp;</td>
		<td><?php echo h($boundary['Boundary']['modified']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $boundary['Boundary']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $boundary['Boundary']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $boundary['Boundary']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $boundary['Boundary']['id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Boundary'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Patients'), array('controller' => 'patients', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Patient'), array('controller' => 'patients', 'action' => 'add')); ?> </li>
	</ul>
</div>
