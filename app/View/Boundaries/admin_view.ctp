<div class="boundaries view">
<h2><?php echo __('Boundary'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($boundary['Boundary']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Patient'); ?></dt>
		<dd>
			<?php echo $this->Html->link($boundary['Patient']['name'], array('controller' => 'patients', 'action' => 'view', $boundary['Patient']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Pulse Current'); ?></dt>
		<dd>
			<?php echo h($boundary['Boundary']['pulse_current']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Pulse Min'); ?></dt>
		<dd>
			<?php echo h($boundary['Boundary']['pulse_min']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Pulse Max'); ?></dt>
		<dd>
			<?php echo h($boundary['Boundary']['pulse_max']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('pressure_systolic Current'); ?></dt>
		<dd>
			<?php echo h($boundary['Boundary']['pressure_systolic_current']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('pressure_systolic Min'); ?></dt>
		<dd>
			<?php echo h($boundary['Boundary']['pressure_systolic_min']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('pressure_systolic Max'); ?></dt>
		<dd>
			<?php echo h($boundary['Boundary']['pressure_systolic_max']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Weight Current'); ?></dt>
		<dd>
			<?php echo h($boundary['Boundary']['weight_current']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Weight Min'); ?></dt>
		<dd>
			<?php echo h($boundary['Boundary']['weight_min']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Weight Max'); ?></dt>
		<dd>
			<?php echo h($boundary['Boundary']['weight_max']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Bmi Current'); ?></dt>
		<dd>
			<?php echo h($boundary['Boundary']['bmi_current']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Bmi Min'); ?></dt>
		<dd>
			<?php echo h($boundary['Boundary']['bmi_min']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Bmi Max'); ?></dt>
		<dd>
			<?php echo h($boundary['Boundary']['bmi_max']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Muscle Mass Current'); ?></dt>
		<dd>
			<?php echo h($boundary['Boundary']['muscle_mass_current']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Muscle Mass Min'); ?></dt>
		<dd>
			<?php echo h($boundary['Boundary']['muscle_mass_min']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Muscle Mass Max'); ?></dt>
		<dd>
			<?php echo h($boundary['Boundary']['muscle_mass_max']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Adipose Tissue Current'); ?></dt>
		<dd>
			<?php echo h($boundary['Boundary']['fat_current']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Adipose Tissue Min'); ?></dt>
		<dd>
			<?php echo h($boundary['Boundary']['fat_min']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Adipose Tissue Max'); ?></dt>
		<dd>
			<?php echo h($boundary['Boundary']['fat_max']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($boundary['Boundary']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($boundary['Boundary']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Boundary'), array('action' => 'edit', $boundary['Boundary']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Boundary'), array('action' => 'delete', $boundary['Boundary']['id']), array(), __('Are you sure you want to delete # %s?', $boundary['Boundary']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Boundaries'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Boundary'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Patients'), array('controller' => 'patients', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Patient'), array('controller' => 'patients', 'action' => 'add')); ?> </li>
	</ul>
</div>
