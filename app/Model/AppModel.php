<?php
/**
 * Application model for CakePHP.
 *
 * This file is application-wide model file. You can put all
 * application-wide model-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Model
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Model', 'Model');

/**
 * Application model for Cake.
 *
 * Add your application-wide methods in the class below, your models
 * will inherit them.
 *
 * @package       app.Model
 */
class AppModel extends Model {

    function afterFind($results, $primary = false){

        if(!isset($results[0])){
            // $results is a single object.
            $results = $this->realAfterFind($results);
        }else{
            //die;
            // $results is a list of objects.
            foreach($results as &$result)
                $result = $this->realAfterFind($result);
        }

        return $results;
    }


    function realAfterFind($object){

        // Cast all values in integer columns to actual integers.
        $columnTypes = $this->getColumnTypes();
        if(isset($object[$this->alias])) {
            foreach ($object[$this->alias] as $columnName => &$value) {
                if (array_key_exists($columnName, $columnTypes)) {
                    //todo: sprawdzic wydajnosc
                    if ($columnTypes[$columnName] === 'integer' && $columnName != "id" && (substr($columnName, -3) != '_id')) {
                        $value = (int)$value;
                    }
                    if ($columnTypes[$columnName] === 'float') {
                        $value = (float)$value;
                    }
                }
            }
        }
        return $object;
    }

}
