<?php
App::uses('Random', "Lib");

App::uses('AppModel', 'Model');
App::uses('BlowfishPasswordHasher', 'Controller/Component/Auth');

class User extends AppModel {
    public $validate = array(
        'username' => array(
            'required' => array(
                'rule' => array('notEmpty'),
                'message' => 'A username is required'
            )
        ),
        'password' => array(
            'required' => array(
                'rule' => array('notEmpty'),
                'message' => 'A password is required'
            )
        ),
        'email' => 'email',
        'role' => array(
            'valid' => array(
                'rule' => array('inList', array('admin', 'doctor')),
                'message' => 'Please enter a valid role',
                'allowEmpty' => false
            )
        )
    );

    public $virtualFields = array(
        'label' => 'CONCAT(User.name, " ", User.surname)'
    );

    public function beforeSave($options = array()) {
        $random = new Random();

        if(!isset($this->data[$this->alias]["token"])){
            $this->data[$this->alias]["token"] = $random->uid();
        }

        if (isset($this->data[$this->alias]['password'])) {
            $passwordHasher = new BlowfishPasswordHasher();
            $this->data[$this->alias]['password'] = $passwordHasher->hash(
                $this->data[$this->alias]['password']
            );
        }
        return true;
    }
}