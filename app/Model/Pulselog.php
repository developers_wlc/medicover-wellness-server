<?php
App::uses('AppModel', 'Model');
/**
 * Pulselog Model
 *
 * @property Patient $Patient
 */
class Pulselog extends AppModel {

    public $actsAs = array('Containable');

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Patient' => array(
			'className' => 'Patient',
			'foreignKey' => 'patient_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
