<?php
App::uses('AppModel', 'Model');
/**
 * Message Model
 *
 * @property Patient $Patient
 * @property Messagetype $Messagetype
 */
class Message extends AppModel {

    public $actsAs = array('Containable');

	//The Associations below have been created with all possible keys, those that are not needed can be removed

    public $validate = array(
        'body' => array(
            'notEmpty' => array(
                'rule' => array('notEmpty'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        )
    );

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Patient' => array(
			'className' => 'Patient',
			'foreignKey' => 'patient_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

    public function beforeSave($options = array()) {
        if (!empty($this->data['Message']['date'])){
            //debug($this->data['Message']['date']);
            $this->data['Message']['date'] = $this->dateFormatBeforeSave($this->data['Message']['date']);
            //debug($this->data['Message']['date']);
            //die;
        }
        return true;
    }

    public function dateFormatBeforeSave($dateString) {
        return gmdate('Y-m-d H:i', strtotime($dateString));
    }
}
