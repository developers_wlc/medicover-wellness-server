<?php
App::uses('AppModel', 'Model');
App::uses('Random', "Lib");

/**
 * Patient Model
 *
 */
class Patient extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';

    public $actsAs = array('Containable');

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'name' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'phone' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'sex' => array(
			'inList' => array(
				'rule' => array('inList', array("male","female")),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
        'pulse_current' => array(
            'decimal' => array(
                'rule' => array('decimal'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'pulse_min' => array(
            'decimal' => array(
                'rule' => array('decimal'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'pulse_max' => array(
            'decimal' => array(
                'rule' => array('decimal'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'pressure_systolic_current' => array(
            'decimal' => array(
                'rule' => array('decimal'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'pressure_systolic_min' => array(
            'decimal' => array(
                'rule' => array('decimal'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'pressure_systolic_max' => array(
            'decimal' => array(
                'rule' => array('decimal'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'weight_current' => array(
            'decimal' => array(
                'rule' => array('decimal'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'weight_min' => array(
            'decimal' => array(
                'rule' => array('decimal'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'weight_max' => array(
            'decimal' => array(
                'rule' => array('decimal'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'bmi_current' => array(
            'decimal' => array(
                'rule' => array('decimal'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'bmi_min' => array(
            'decimal' => array(
                'rule' => array('decimal'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'bmi_max' => array(
            'decimal' => array(
                'rule' => array('decimal'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'muscle_mass_current' => array(
            'decimal' => array(
                'rule' => array('decimal'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'muscle_mass_min' => array(
            'decimal' => array(
                'rule' => array('decimal'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'muscle_mass_max' => array(
            'decimal' => array(
                'rule' => array('decimal'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'fat_current' => array(
            'decimal' => array(
                'rule' => array('decimal'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'fat_min' => array(
            'decimal' => array(
                'rule' => array('decimal'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'fat_max' => array(
            'decimal' => array(
                'rule' => array('decimal'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
	);

    public $hasMany = array(
        'Boundaries' => array(
            'className' => 'Boundary',
        ),
        'Messages' => array(
            'className' => 'Message',
        ),
        'Distancelogs' => array(
            'className' => 'Distancelog',
        ),
        "Calorielogs" => array(
            "className" => "Calorielog"
        ),
        "Pulselogs" => array(
            "className" => "Pulselog",
        )
    );

    public $belongsTo = array(
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'user_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

    public $virtualFields = array(
        'label' => 'CONCAT(Patient.name, " ", Patient.surname)'
    );

    public function findByConditions($conditions, $contain = array("Boundaries"=>array('limit'=>1, 'order' => 'Boundaries.created DESC')) ){
        $options = array('conditions' => $conditions,
            "contain"=>$contain);
        $patient = $this->find('first', $options);

        if(!$patient){
            return null;
        }

        $patient["CurrentBoundary"] = $patient["Boundaries"][0];

        $formatedBoundary = array();

        function endsWith($haystack, $needle) {
            // search forward starting from end minus needle length characters
            return $needle === "" || (($temp = strlen($haystack) - strlen($needle)) >= 0 && strpos($haystack, $needle, $temp) !== FALSE);
        }

        function splitSuffix(&$array, $key, $val, $suffix){
            if(endsWith($key, $suffix)=== true){
                $name = substr($key, 0, -(strlen($suffix)+1));
                $array[$name][$suffix] = $val;
            }
        }

        foreach($patient["CurrentBoundary"] as $key=>$boundary){
            splitSuffix($formatedBoundary, $key, $boundary, "min");
            splitSuffix($formatedBoundary, $key, $boundary, "max");
            splitSuffix($formatedBoundary, $key, $boundary, "current");
        }

        $patient["FormattedBoundary"] = $formatedBoundary;

        return $patient;
    }

    public function findByAccessToken($accesstoken){
        return $this->findByConditions(array("Patient.access_token" => $accesstoken));
    }

    public function findByTheId($id){
        $a = $this->findByConditions(array("Patient.id" => $id));
        //var_dump($a);
        //die;
        return $a;
    }

    public function beforeSave($options = array()) {
        $random = new Random();

        if(!isset($this->data[$this->alias]["token"])){
            $this->data[$this->alias]["token"] = $random->uid();
        }

        if (isset($this->data[$this->alias]['password'])) {
            $passwordHasher = new BlowfishPasswordHasher();
            $this->data[$this->alias]['password'] = $passwordHasher->hash(
                $this->data[$this->alias]['password']
            );
        }
        return true;
    }

    /*function realAfterFind($object){

        // Cast all values in integer columns to actual integers.
        $columnTypes = $this->getColumnTypes();
        if(isset($object[$this->name]))
            foreach($object[$this->name] as $columnName => &$value)
                if(array_key_exists($columnName, $columnTypes)) {
                    if ($columnTypes[$columnName] === 'integer')
                        $value = (int)$value;
                }

        return $object;
    }*/
}
