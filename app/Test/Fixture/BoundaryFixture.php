<?php
/**
 * BoundaryFixture
 *
 */
class BoundaryFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'patient_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'pulse_current' => array('type' => 'float', 'null' => true, 'default' => null, 'unsigned' => false),
		'pulse_min' => array('type' => 'float', 'null' => true, 'default' => null, 'unsigned' => false),
		'pulse_max' => array('type' => 'float', 'null' => true, 'default' => null, 'unsigned' => false),
		'pressure_systolic_current' => array('type' => 'float', 'null' => true, 'default' => null, 'unsigned' => false),
		'pressure_systolic_min' => array('type' => 'float', 'null' => true, 'default' => null, 'unsigned' => false),
		'pressure_systolic_max' => array('type' => 'float', 'null' => true, 'default' => null, 'unsigned' => false),
		'weight_current' => array('type' => 'float', 'null' => true, 'default' => null, 'unsigned' => false),
		'weight_min' => array('type' => 'float', 'null' => true, 'default' => null, 'unsigned' => false),
		'weight_max' => array('type' => 'float', 'null' => true, 'default' => null, 'unsigned' => false),
		'bmi_current' => array('type' => 'float', 'null' => true, 'default' => null, 'unsigned' => false),
		'bmi_min' => array('type' => 'float', 'null' => true, 'default' => null, 'unsigned' => false),
		'bmi_max' => array('type' => 'float', 'null' => true, 'default' => null, 'unsigned' => false),
		'muscle_mass_current' => array('type' => 'float', 'null' => true, 'default' => null, 'unsigned' => false),
		'muscle_mass_min' => array('type' => 'float', 'null' => true, 'default' => null, 'unsigned' => false),
		'muscle_mass_max' => array('type' => 'float', 'null' => true, 'default' => null, 'unsigned' => false),
		'fat_current' => array('type' => 'float', 'null' => true, 'default' => null, 'unsigned' => false),
		'fat_min' => array('type' => 'float', 'null' => true, 'default' => null, 'unsigned' => false),
		'fat_max' => array('type' => 'float', 'null' => true, 'default' => null, 'unsigned' => false),
		'created' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB', 'comment' => 'boudary data for user')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'patient_id' => 1,
			'pulse_current' => 1,
			'pulse_min' => 1,
			'pulse_max' => 1,
			'pressure_systolic_current' => 1,
			'pressure_systolic_min' => 1,
			'pressure_systolic_max' => 1,
			'weight_current' => 1,
			'weight_min' => 1,
			'weight_max' => 1,
			'bmi_current' => 1,
			'bmi_min' => 1,
			'bmi_max' => 1,
			'muscle_mass_current' => 1,
			'muscle_mass_min' => 1,
			'muscle_mass_max' => 1,
			'fat_current' => 1,
			'fat_min' => 1,
			'fat_max' => 1,
			'created' => '2015-05-29 14:47:36',
			'modified' => '2015-05-29 14:47:36'
		),
	);

}
