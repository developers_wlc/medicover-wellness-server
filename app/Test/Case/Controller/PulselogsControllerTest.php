<?php
App::uses('PulselogsController', 'Controller');

/**
 * PulselogsController Test Case
 *
 */
class PulselogsControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.pulselog',
		'app.patient',
		'app.boundary'
	);

}
