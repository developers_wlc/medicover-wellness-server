<?php
App::uses('Calorielog', 'Model');

/**
 * Calorielog Test Case
 *
 */
class CalorielogTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.calorielog',
		'app.patient',
		'app.boundary',
		'app.pulselog'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Calorielog = ClassRegistry::init('Calorielog');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Calorielog);

		parent::tearDown();
	}

}
