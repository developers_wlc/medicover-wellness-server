<?php
App::uses('Distancelog', 'Model');

/**
 * Distancelog Test Case
 *
 */
class DistancelogTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.distancelog',
		'app.patient',
		'app.boundary',
		'app.pulselog'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Distancelog = ClassRegistry::init('Distancelog');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Distancelog);

		parent::tearDown();
	}

}
