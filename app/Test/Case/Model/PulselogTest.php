<?php
App::uses('Pulselog', 'Model');

/**
 * Pulselog Test Case
 *
 */
class PulselogTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.pulselog',
		'app.patient',
		'app.boundary'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Pulselog = ClassRegistry::init('Pulselog');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Pulselog);

		parent::tearDown();
	}

}
