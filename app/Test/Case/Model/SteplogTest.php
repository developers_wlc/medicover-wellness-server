<?php
App::uses('Steplog', 'Model');

/**
 * Steplog Test Case
 *
 */
class SteplogTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.steplog',
		'app.patient',
		'app.boundary',
		'app.pulselog'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Steplog = ClassRegistry::init('Steplog');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Steplog);

		parent::tearDown();
	}

}
