<?php
App::uses('Messagetype', 'Model');

/**
 * Messagetype Test Case
 *
 */
class MessagetypeTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.messagetype',
		'app.message'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Messagetype = ClassRegistry::init('Messagetype');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Messagetype);

		parent::tearDown();
	}

}
