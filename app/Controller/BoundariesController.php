<?php
App::uses('AppController', 'Controller');
/**
 * Boundaries Controller
 *
 * @property Boundary $Boundary
 * @property PaginatorComponent $Paginator
 */
class BoundariesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Boundary->recursive = 0;
		$this->set('boundaries', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Boundary->exists($id)) {
			throw new NotFoundException(__('Invalid boundary'));
		}
		$options = array('conditions' => array('Boundary.' . $this->Boundary->primaryKey => $id));
		$this->set('boundary', $this->Boundary->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Boundary->create();
			if ($this->Boundary->save($this->request->data)) {
				$this->Session->setFlash(__('The boundary has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The boundary could not be saved. Please, try again.'));
			}
		}
		$patients = $this->Boundary->Patient->find('list');
		$this->set(compact('patients'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Boundary->exists($id)) {
			throw new NotFoundException(__('Invalid boundary'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Boundary->save($this->request->data)) {
				$this->Session->setFlash(__('The boundary has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The boundary could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Boundary.' . $this->Boundary->primaryKey => $id));
			$this->request->data = $this->Boundary->find('first', $options);
		}
		$patients = $this->Boundary->Patient->find('list');
		$this->set(compact('patients'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Boundary->id = $id;
		if (!$this->Boundary->exists()) {
			throw new NotFoundException(__('Invalid boundary'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Boundary->delete()) {
			$this->Session->setFlash(__('The boundary has been deleted.'));
		} else {
			$this->Session->setFlash(__('The boundary could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
