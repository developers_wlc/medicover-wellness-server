<?php
App::uses('AppController', 'Controller');
App::uses('ApiErrorException', 'Lib/Exception');

App::import('Vendor', 'ApnsPHP', array('file' => 'ApnsPHP/Autoload.php'));
/**
 * Patients Controller
 *
 * @property Patient $Patient
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class PatientsController extends AppController {

/**
 * Components
 *
 * @var array
 */

    public $actsAs = array('Containable');

    public $uses = array("Patient", "Message", "Messagetype");
	public $components = array('Paginator', 'Session', 'RequestHandler');

/**
 * admin_index method
 *
 * @return void
 */
	public function index() {
		$this->Patient->recursive = 0;
        if(!$this->isAdmin()){
            $this->Paginator->settings = array(
                'conditions' => array('Patient.user_id' => $this->user["id"])
            );
        }
        $this->set("patientsHasPages", ($this->params['paging']['Patient']['pageCount'] > 1));
		$this->set('patients', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null, $messagetype = null) {

		if (!$this->Patient->exists($id)) {
			throw new NotFoundException(__('Invalid patient'));
		}

        //$p = $this->Patient->findByTheId($id);
        $p = $this->Patient->findByConditions(array("Patient.id" => $id), array(
            "Boundaries"=>array('limit'=>1, 'order' => 'Boundaries.created DESC'),
        ));

		$this->set('patient', $p);

        $patients = $this->Patient->find("all", array(
            "conditions" => array("Patient.id"=>$id)));

        //debug($patients);
        //die;
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
            //debug($this->request->data);die;
            $this->request->data["Patient"]["user_id"] = $this->user["id"];
			$this->Patient->create();
			if ($this->Patient->saveAssociated($this->request->data)) {
				$this->Session->setFlash(__('The patient has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The patient could not be saved. Please, try again.'));
			}
		}
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Patient->exists($id)) {
			throw new NotFoundException(__('Invalid patient'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Patient->saveAssociated($this->request->data)) {
				$this->Session->setFlash(__('The patient has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The patient could not be saved. Please, try again.'));
			}
		} else {
			//$options = array('conditions' => array('Patient.' . $this->Patient->primaryKey => $id));
            $patient = $this->Patient->findByTheId($id);
            //var_dump($patient); die;

			$this->request->data = $patient;
		}
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Patient->id = $id;
		if (!$this->Patient->exists()) {
			throw new NotFoundException(__('Invalid patient'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Patient->delete()) {
			$this->Session->setFlash(__('The patient has been deleted.'));
		} else {
			$this->Session->setFlash(__('The patient could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

    function beforeFilter()
    {
        parent::beforeFilter();
        $this->Security->unlockedActions = array('push');
    }

    public function push(){

        $this->autoLayout = false;
        $this->response->type('json');
        $this->RequestHandler->renderAs($this, 'json');

        if($this->request->is('ajax')){
            $inputData = $this->request->data;
            if(isset($inputData["body"]) && strlen($inputData["body"])<= 0){
                throw new BadRequestException();
            }
            $patient = $this->Patient->find("first", array("recursive"=>-1,  "conditions"=>array("id"=>$inputData["patient_id"])));
            if (!$patient) {
                throw new NotFoundException(__('Invalid patient'));
            }
            if($patient["Patient"]["ios_token"]){
                $settings = Configure::read("Push.ios");
                date_default_timezone_set('Europe/Rome');
                error_reporting(-1);

                $push = new ApnsPHP_Push(
                    ApnsPHP_Abstract::ENVIRONMENT_SANDBOX,
                    '../Certs/'.$settings["provider"]
                );
                $push->setRootCertificationAuthority('../Certs/'.$settings["root_certification_authority"]);
                $push->setLogger(new ApnsPHP_Log_Fake());
                $push->connect();
                $message = new ApnsPHP_Message($patient["Patient"]["ios_token"]);
                //$message->setCustomIdentifier("Message-Badge-3");
                $message->setCategory('notification');
                $message->setBadge(1);
                $message->setText($inputData["body"]);
                $message->setSound();
                $push->add($message);
                $push->send();
                $push->disconnect();
                $aErrorQueue = $push->getErrors();

                if (!empty($aErrorQueue)) {
                    throw new ApiErrorException(array(
                        "message"=> "could not sent message",
                        "title"=> __("apns", true)
                    ), 400);
                } else {
                    $this->set('output', array("status"=>"ok"));
                    $this->set('_serialize', 'output');
                }

            } else{
                throw new NotFoundException(__('patient has no ios token'));
            }
        }else{
            throw new BadRequestException();
        }
    }
}
