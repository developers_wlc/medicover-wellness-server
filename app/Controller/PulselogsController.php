<?php
App::uses('AppController', 'Controller');
/**
 * Pulselogs Controller
 *
 * @property Pulselog $Pulselog
 * @property PaginatorComponent $Paginator
 */
class PulselogsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

}
