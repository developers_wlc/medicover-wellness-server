<?php
App::uses('AppController', 'Controller');
App::uses('ApiErrorException', 'Lib/Exception');

App::uses('Random', "Lib");

App::uses('Folder', 'Utility');
App::uses('File', 'Utility');

//App::uses('Jsv4', 'Vendor');
App::import('Vendor', 'Jsv4', array('file' => 'jsv4.php'));

class ApiController extends AppController {

    public $uses = array('Patient', "Pulselog", "Steplog", "Distancelog", "Calorielog", "Message");
    public $components = array('Paginator', 'Session', "RequestHandler", "PatientFormatter");

    private $patient;
    private $requestData = null;

    private function response($data){
        $this->set('output', array("content"=>$data));
    }

    private function validateRequest(){
        $functionName = $this->request->params['action'];
        $file = new File("../Schemas/Api/".$functionName.".json");
        $json = $file->read(true, 'r');
        $valResult = Jsv4::validate($this->requestData,json_decode($json));
        if(!$valResult->valid){
            throw new ApiErrorException(array(
                "message"=> $valResult->errors[0]->dataPath. " " . $valResult->errors[0]->message,
                "title"=> __("wrong data", true)
            ), 400);
        }
        return $valResult;
    }

    private function authToken(){
        $access_token = $this->request->header('access-token');
        if($access_token){
            $patient = $this->Patient->findByAccessToken($access_token);
            if($patient){
                $this->patient = $patient;
                $this->PatientFormatter->setPatient($this->patient);
                return null;
            }
        }

        throw new ApiErrorException(array(
            "message"=> __("unauthorized", true),
            "title"=> __("unauthorized", true)
        ), 401);
    }

    private function authAjax(){
        //just veeery simple authorization :)
        if($this->isLoggedIn()){
            if($this->request->is('ajax')){
                //all ok go
            } else {
                throw new ApiErrorException(array(
                    "message"=> "wrong query",
                    "title"=> __("wrong data", true)
                ), 400);
            }
        } else {
            throw new ApiErrorException(array(
                "message"=> __("unauthorized", true),
                "title"=> __("unauthorized", true)
            ), 401);
        }
    }

    public function beforeFilter() {
        parent::beforeFilter();

        $this->Security->unlockedActions = array('login', "push_consent", "sync_data", "sync_messages", "autocomplete");

        $this->response->type('json');

        switch ($this->request->params['action']) {
            case "login":
                break;
            case "autocomplete":
                $this->authAjax();
                break;
            case "push":
                //$this->authAjax();
                break;
            default :
                $this->authToken();
                break;
        }

        $this->autoLayout = false;
        $this->Auth->allow();

        $this->RequestHandler->renderAs($this, 'json');

        if($this->request->is('post')) {

            $jsonData = $this->request->input('json_decode');

            if (!$jsonData) {
                throw new ApiErrorException(array(
                    "message" => __("wrong json data", true),
                    "title" => __("wrong json data", true)
                ), 400);
            } else {
                $this->requestData = $jsonData;
            }

            $this->validateRequest();
        }
    }

    public function login(){

        $conditions = array("Patient.name" => $this->requestData->name, "Patient.token" => $this->requestData->token);
        $patient = $this->Patient->find('first', array('conditions' => $conditions));

        if(!$patient){
            throw new ApiErrorException(array(
                "message"=>__("not found", true),
                "title"=> __("not found", true)
            ), 404);
        }

        $patient["Patient"]["access_token"] = Random::uid(32);
        //$patient["Patient"]["consent_push"] = $this->requestData->consent;

        $updatedPatient = $this->Patient->save($patient);
        $this->response(array("access_token"=>$updatedPatient["Patient"]["access_token"]));
    }

    public function push_consent(){

        $this->patient["Patient"]["consent_push"] = $this->requestData->consent;
        if(isset($this->requestData->iostoken)) {
            $this->patient["Patient"]["ios_token"] = $this->requestData->iostoken;
        }

        if(!$this->Patient->save($this->patient)){
            throw new ApiErrorException(array(
                "message"=>__("could not save data", true),
                "title"=> __("could not save data", true)
            ), 500);
        }
        $this->response("");
    }

    private function saveSideBoundary($data, $model, $patient){
        if(isset($data)){

            $res = array_map(function($item) use ($patient){
                $item->patient_id = $patient["id"];
                return $item;
            }, $data);
            $model->saveAll($res);
        }
    }

    public function sync_data(){

        $inputData = $this->requestData->content;

        $patient = $this->patient["Patient"];

        //deal with input data

        if(isset($inputData->pulses)) {
            $this->saveSideBoundary($inputData->pulses, $this->Pulselog, $patient);
        }
        if(isset($inputData->distances)) {
            $this->saveSideBoundary($inputData->distances, $this->Distancelog, $patient);
        }
        if(isset($inputData->calories)) {
            $this->saveSideBoundary($inputData->calories, $this->Calorielog, $patient);
        }
        if(isset($inputData->steps)) {
            $this->saveSideBoundary($inputData->steps, $this->Steplog, $patient);
        }

        //prepare response

        $response = $this->PatientFormatter->syncResponse();

        $this->response($response);
    }

    public function sync_messages(){

        $inputData = $this->requestData;

        $patient = $this->patient["Patient"];

        $messages = $this->Message->find("all", array(
            "recursive"=>-1,
            "conditions"=>array(
                "patient_id" => $patient["id"],
                "Message.sent "=> null
            ),
            'order' => array('Message.created ASC'),
        ));

        //debug($messages);

        if(empty($messages)){
            return $this->response($messages);
        }

        $timestamp = time()+date("Z");
        $gmNow = gmdate("Y-m-d H:i:s",$timestamp);

        foreach($messages as $key => $message){
            $messages[$key]["Message"]['sent'] = $gmNow;
            $messages[$key]["Message"]['date'] = $gmNow;
        }

        //debug($messages);

        if($this->Message->saveAll($messages)){
        }

        $messageType= array_flip(Configure::read('Message.types'));

        $cleanMessages= array();

        foreach($messages as $key => $message){

            array_push($cleanMessages, array(
                "messagetype_name"=> $messageType[$message["Message"]["messagetype"]],
                "body"=>$message["Message"]["body"],
                "created"=> $message["Message"]["created"]
            ));
        }

        $this->response($cleanMessages);
        //test
    }

    public function autocomplete(){
        $inputData = $this->request;
        $query = $inputData->query("s");

        $query = preg_split('/\s+/', $query);

        $conditions = array();

        if(!$this->isAdmin()){
            $conditions = array("and"=>array("user_id"=>$this->user["id"]));
        }

        foreach ($query as $item) {
            $item = trim($item);
            $conditions["and"][] = array(
                "or"=>array(
                    'Patient.name LIKE' => "%$item%",
                    'Patient.surname LIKE' => "%$item%",
                    'Patient.phone LIKE' => "%$item%"
                )
            );
        }

        $patients = $this->Patient->find("all", array(
            "fields"=>array("name", "surname", "id", "user_id"),
            "recursive"=>-1,
            "conditions"=> $conditions));


        $response = array();
        foreach($patients as $patient){
            $response[]=array(
                "label"=> $patient["Patient"]["name"]. " ". $patient["Patient"]["surname"],
                "value"=> $patient["Patient"]["id"]
            );
        }
        $this->response($response);
    }

    public function beforeRender() {
        parent::beforeRender();
        $this->set('_serialize', 'output');
    }
}