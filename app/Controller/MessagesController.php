<?php
App::uses('AppController', 'Controller');
/**
 * Messages Controller
 *
 * @property Message $Message
 * @property PaginatorComponent $Paginator
 */
class MessagesController extends AppController {

    public $uses = array("Patient", "Message", "Messagetype");
    public $components = array('Paginator', 'Session');
    public $helpers = array('Form', 'Html', 'Utc');

    private function setMessageType($messagetype){
        $this->Messagetype->recursive = -1;
        $messageTypes = Configure::read("Message.types");
        $this->set("messageTypes", array_flip($messageTypes));
        //$selectedMessageType = array_shift($messageTypes)["Messagetype"]["id"];

        $selectedMessageType = $messageTypes["health"];

        if($messagetype) {
            $selectedMessageType = $messagetype;
        }

        $this->set("selectedMessageType", $selectedMessageType);
        return $selectedMessageType;
    }

    public function user($id = null, $messagetype = null) {
        //todo: dodac jakich achtung ze zapisano pomyslnie
        if ($this->request->is('post')) {
            $this->data["Message"]["source"] = Configure::read("Message.sources")["doctor"];
            $this->Message->save($this->data);
            $this->data = null;
        }

        if (!$this->Patient->exists($id)) {
            throw new NotFoundException(__('Invalid patient'));
        }

        $p = $this->Patient->findByConditions(array("Patient.id" => $id), array(
            "Boundaries"=>array('limit'=>1, 'order' => 'Boundaries.created DESC'),
        ));

        $this->set('patient', $p);

        $this->Message->recursive = -1;
        $this->Paginator->settings = array(
            "order"=> "Message.created DESC",
            "limit"=> 5,
            'conditions' => array(
                'Message.patient_id' => $p["Patient"]["id"],
                "Message.messagetype" => $this->setMessageType($messagetype)
            )
        );

        $m = $this->Paginator->paginate("Message");
        $this->set("messageHasPages", ($this->params['paging']['Message']['pageCount'] > 1));
        $this->set('messages', $m);
    }
/**
 * admin_index method
 *
 * @return void
 */
	public function index($messagetype = null) {

        $this->Message->recursive = 0;
        $conditions = array(
            "Message.messagetype" => $this->setMessageType($messagetype)
        );
        if(!$this->isAdmin()){
            $conditions["Patient.user_id"] = $this->user["id"];
        }
        $this->Paginator->settings = array(
            "order"=> "Message.created DESC",
            "limit"=> 5,
            'conditions' => $conditions
        );

        $this->set("hasPages", ($this->params['paging']['Message']['pageCount'] > 1));
        $this->set('messages', $this->Paginator->paginate("Message"));


		/*$this->Message->recursive = 0;
        if(!$this->isAdmin()){
            $this->Paginator->settings = array(
                'conditions' => array('Patient.user_id' => $this->user["id"])
            );
        }
        $this->set("hasPages", ($this->params['paging']['Message']['pageCount'] > 1));
		$this->set('messages', $this->Paginator->paginate("Message"));*/
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Message->exists($id)) {
			throw new NotFoundException(__('Invalid message'));
		}
		$options = array('conditions' => array('Message.' . $this->Message->primaryKey => $id));
		$this->set('message', $this->Message->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Message->create();
			if ($this->Message->save($this->request->data)) {
				$this->Session->setFlash(__('The message has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The message could not be saved. Please, try again.'));
			}
		}
		$patients = $this->Message->Patient->find('list');
		$messagetypes = $this->Message->Messagetype->find('list');
		$this->set(compact('patients', 'messagetypes'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Message->exists($id)) {
			throw new NotFoundException(__('Invalid message'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Message->save($this->request->data)) {
				$this->Session->setFlash(__('The message has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The message could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Message.' . $this->Message->primaryKey => $id));
			$this->request->data = $this->Message->find('first', $options);
		}
		$patients = $this->Message->Patient->find('list');
		$messagetypes = $this->Message->Messagetype->find('list');
		$this->set(compact('patients', 'messagetypes'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Message->id = $id;
		if (!$this->Message->exists()) {
			throw new NotFoundException(__('Invalid message'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Message->delete()) {
			$this->Session->setFlash(__('The message has been deleted.'));
		} else {
			$this->Session->setFlash(__('The message could not be deleted. Please, try again.'));
		}
		return $this->redirect($this->referer());
	}
}
