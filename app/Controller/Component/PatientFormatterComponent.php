<?php
App::uses('Component', 'Controller');
class PatientFormatterComponent extends Component {
    // the other component your component uses
    //public $components = array('Existing');

    private $patient = null;

    public function setPatient($patient) {
        $this->patient = $patient;
    }

    //jesli dane nie mają ustawione min lub max to nie chcemy ich w odpowiedzi
    private function isBoundarySet($boundary){
        if($boundary["min"] != 0 && $boundary["max"] != 0){
            return true;
        }else {
            return false;
        }
    }

    public function syncResponse(){

        $fb =  $this->patient["FormattedBoundary"];

        $response["health"] = $fb;

        if($this->isBoundarySet($fb["weather_humidity"])){
            $response["weather"]["humidity"] = $fb["weather_humidity"];
        }
        if($this->isBoundarySet($fb["weather_temperature"])){
            $response["weather"]["temperature"] = $fb["weather_temperature"];
        }
        if($this->isBoundarySet($fb["weather_pressure"])){
            $response["weather"]["pressure"] = $fb["weather_pressure"];
        }

        unset($response["health"]["weather_humidity"]);
        unset($response["health"]["weather_temperature"]);
        unset($response["health"]["weather_pressure"]);

        if($this->isBoundarySet($fb["calorie"])){
            $response["fitness"]["calorie"] = $fb["calorie"];
        }
        if($this->isBoundarySet($fb["step"])){
            $response["fitness"]["step"] = $fb["step"];
        }
        if($this->isBoundarySet($fb["distance"])){
            $response["fitness"]["distance"] = $fb["distance"];
        }

        unset($response["health"]["calorie"]);
        unset($response["health"]["step"]);
        unset($response["health"]["distance"]);

        return $response;
    }
}
?>