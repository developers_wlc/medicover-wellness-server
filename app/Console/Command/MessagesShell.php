<?php
App::uses('CakeEmail', 'Network/Email');

class MessagesShell extends AppShell {

    public $uses = array('Patient', "Message");

    private $yesterdayUtc;

    private function addMessage($message){
        $this->Message->save($message);
    }

    private function sendEmail($message){
        if(empty($message["doctor"]["email"])){
            return false;
        }

        $Email = new CakeEmail("default");
        $Email->template('medicover', 'medicover')
            ->emailFormat('html')
            ->viewVars(array("message"=>$message))
            ->helpers("Html")
            ->subject(__("Stan pacjenta")." ".$message["patient"]["label"])
            ->to($message["doctor"]["email"])
            ->from(array('medicover.wellness@welikecaps.com' => 'Medicover Wellness Message System'))
            ->send();

        $Email = new CakeEmail("default");
        $Email->template('medicover', 'medicover')
            ->emailFormat('html')
            ->viewVars(array("message"=>$message))
            ->helpers("Html")
            ->subject(__("Stan pacjenta")." ".$message["patient"]["label"])
            ->to("adam@kalicinscy.com")
            ->from(array('medicover.wellness@welikecaps.com' => 'Medicover Wellness Message System'))
            ->send();
    }

    private function nameToTypeId($name){
        $typename = Configure::read("Message.nameToType")[$name];
        return Configure::read("Message.types")[$typename];
    }

    private function checkBoundary($names, $patient){
        foreach($names as $name) {

            $logsName = $name."logs";
            $lname = strtolower($name);

            if (empty($patient[$logsName])) {
                return false;
            }

            $pulseMin = $patient["Boundaries"][0][$lname . "_min"];
            $pulseMax = $patient["Boundaries"][0][$lname . "_max"];

            //na przyszłość jak będzie więcej niż jedna dana dziennie
            foreach ($patient[$logsName] as $item) {
                $message = array(
                    "statusDateUtc"=>$this->yesterdayUtc,
                    "doctor"=> $patient["User"],
                    "patient"=> $patient["Patient"],
                    "patient_id" => $patient["Patient"]["id"],
                    "messagetype" => $this->nameToTypeId($lname),
                    "source" => Configure::read("Message.sources")["system"],
                    "value" => $item["value"]
                );
                if ($item["value"] <= $pulseMin) {
                    //todo: translate
                    $message["body"] = __("message-" . $lname . "-too-low");
                    $message["reason"] = $lname . "-low";
                    $this->addMessage($message);
                    $this->sendEmail($message);
                } else if ($item["value"] >= $pulseMax) {
                    //todo: translate
                    $message["body"] = __("message-" . $lname . "-too-high");
                    $message["reason"] = $lname . "-high";
                    $this->addMessage($message);
                    $this->sendEmail($message);
                }
            }
        }
    }

    public function main() {
        $this->yesterdayUtc = gmdate('Y-m-d', strtotime("yesterday"));
        debug($this->yesterdayUtc);
        $patients = $this->Patient->find("all", array(
            "conditions" => array("Patient.id"=>21),
            "contain"=>array(
                "User",
                "Boundaries"=>array('limit'=>1, 'order' => 'Boundaries.created DESC'),
                "Distancelogs"=>array("conditions" => array("Distancelogs.timestamp >="=> $this->yesterdayUtc)),
                "Pulselogs"=>array("conditions" => array("Pulselogs.timestamp >="=> $this->yesterdayUtc)),
                "Calorielogs"=>array("conditions" => array("Calorielogs.timestamp >="=> $this->yesterdayUtc))
            )));

        debug($patients);

        foreach($patients as $patient){
            $this->checkBoundary(array("Pulse", "Calorie", "Distance", "Step"), $patient);
        }
        //test2
    }
}
