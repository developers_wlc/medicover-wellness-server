<?php

App::import('Vendor', 'ApnsPHP', array('file' => 'ApnsPHP/Autoload.php'));

class PushShell extends AppShell {

    public $uses = array('Patient');

    private $push = null;

    function initialize() {
        $settings = Configure::read("Push.ios");
        date_default_timezone_set('Europe/Rome');
        error_reporting(-1);

        $this->push = new ApnsPHP_Push(
            ApnsPHP_Abstract::ENVIRONMENT_SANDBOX,
            'Certs/'.$settings["provider"]
        );
        $this->push->setRootCertificationAuthority('Certs/'.$settings["root_certification_authority"]);
    }


    public function feedback(){
        $settings = Configure::read("Push.ios");

// Adjust to your timezone
        date_default_timezone_set('Europe/Rome');
// Report all PHP errors
        error_reporting(-1);

// Instanciate a new ApnsPHP_Feedback object
        $feedback = new ApnsPHP_Feedback(
            ApnsPHP_Abstract::ENVIRONMENT_SANDBOX,
            'Certs/'.$settings["provider"]
        );
// Connect to the Apple Push Notification Feedback Service
        $feedback->connect();
        $aDeviceTokens = $feedback->receive();
        if (!empty($aDeviceTokens)) {
            var_dump($aDeviceTokens);
        }
// Disconnect from the Apple Push Notification Feedback Service
        $feedback->disconnect();
    }

    public function one(){
        $this->push->connect();
        $message = new ApnsPHP_Message('a101be70af2c8848f407fc37c5fd838955d97d9191ee94390c63276c37411c50');
        $message->setCustomIdentifier("Message-Badge-3");
        $message->setCategory('notification');
        $message->setBadge(3);
        $message->setText('Hello');
        $message->setSound();
        $message->setExpiry(30);
        $this->push->add($message);
        $this->push->send();
        $this->push->disconnect();
        $aErrorQueue = $this->push->getErrors();
        if (!empty($aErrorQueue)) {
            var_dump($aErrorQueue);
        }
    }

    public function chunks(){
        Configure::write('debug', 2);
        $patients = $this->Patient->find("all", array(
            "fields"=>array("ios_token"),
            "conditions"=>array(
                "not"=>array('ios_token'=>null)
            )));
        $tokens = Hash::extract($patients, '{n}.Patient.ios_token');
        if (count($tokens)) {

            $chunks = array_chunk($tokens, 50);
            foreach ($chunks as $key => $chunk) {
                $this->log("Sending chunk no. " . $key);
                // Connect to the Apple Push Notification Service
                $this->push->connect();

                foreach ($chunk as $token) {
                    try {
                        $message = new ApnsPHP_Message($token);
                        $message->setCustomIdentifier("Message-Badge-3");
                        $message->setCategory('notification');
                        $message->setBadge(3);
                        $message->setText('Hello');
                        $message->setSound();
                        $message->setExpiry(30);
                        $this->push->add($message);
                    } catch (ApnsPHP_Message_Exception $e) {
                        $this->log("really fucked up token: " . $token);
                    }
                }
                $this->push->send();
                $this->push->disconnect();
                $aErrorQueue = $this->push->getErrors();
                if (!empty($aErrorQueue)) {
                    foreach ($aErrorQueue as $er)
                        foreach ($er["MESSAGE"]->_aDeviceTokens as $wrongToken) {
                            echo $wrongToken . "\n";
                            $this->log("wrongtoken: " . $wrongToken);
                        }
                }
                $this->log("Sent chunk no. " . $key);
            }
        }
    }

    public function main() {
        Configure::write('debug', 2);
        $this->chunks();

        $settings = Configure::read("Push.ios");
        date_default_timezone_set('Europe/Rome');
        error_reporting(-1);

        $server = new ApnsPHP_Push_Server(
            ApnsPHP_Abstract::ENVIRONMENT_SANDBOX,
            'Certs/'.$settings["provider"]
        );

        $server->setRootCertificationAuthority('Certs/'.$settings["root_certification_authority"]);
        $server->setProcesses(2);
        $server->start();

        $tokens = array("a101be70af2c8848f407fc37c5fd838955d97d9191ee94390c63276c37411c50", "0fa8609064d20f31a8e2c3f51d774ff5c2944675f76365f7ddd1205364d66025");

        $tokensCounter = count($tokens);
        $i = 0;
        while ($server->run()) {

            $aErrorQueue = $server->getErrors();
            if (!empty($aErrorQueue)) {
                var_dump($aErrorQueue);
            }
            if ($i < $tokensCounter) {
                $message = new ApnsPHP_Message($tokens[$i]);
                $message->setCustomIdentifier("Message-Badge-3");
                $message->setCategory("notifications");
                $message->setBadge(3);
                $message->setText('test medicover');
                $message->setSound();
                //$message->setCustomProperty('acme2', array('bang', 'whiz'));
                //$message->setCustomProperty('acme3', array('bing', 'bong'));
                $message->setExpiry(30);
                $server->add($message);
                $i++;
            }
            usleep(200000);
            $q = $server->getQueue();
            if(!$q){
                $server->getQueue();
            }
        }
    }
}
