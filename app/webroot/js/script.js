
var messageLimit = {
    init: function(messageName, counterName, limit){
        var maxChars = limit;

        function limitText(field, maxChar){
            var ref = $(field),
                val = ref.val();
            if ( val.length >= maxChar ){
                ref.val(function() {
                    return val.substr(0, maxChar);
                });
            }
            $(counterName).html(maxChar - ref.val().length);
        }
        if($(messageName).length) {
            limitText($(messageName), maxChars);
            $(messageName).on('keyup', function () {
                limitText(this, maxChars)
            });
        }
    }
}

var messageDate = {
    init: function(){

        var nowCeilSeconds = function(seconds){
            var dateNow = new Date(Math.ceil((new Date().getTime()/1000/seconds) )* seconds*1000);
            return moment(dateNow).format()
        };

        $('#MessageDate').val(nowCeilSeconds(3600));

        $('#MessageDate').datetimepicker({
            //format:'Y-m-d H:i',
            validateOnBlur: false,
            inline:true,
            lang:'pl',
            mask:false,
            defaultSelect: false,
            roundTime: "ceil",
            onChangeDateTime: function(dp,$input) {
                $input.val(moment(dp).format());
            }
        });
    }
}

var time = {
    utcToLocal: function(classname){
        $("."+classname ).each(function( index ) {
            var date = new Date($( this ).attr("date") + " UTC");
            var localDate = moment(date).format("YYYY-MM-DD HH:mm");
            $( this ).html(localDate);
        });
    }
}

var modal = {
    init: function(name){
        var modal = $(name);
        modal.on('hide.bs.modal', function (event) {
            $(this).find("textarea").val("");
        });
        modal.on('show.bs.modal', function (event) {

        });
        modal.find(".submitajax").click(function(){
            $form = modal.find("form");
            console.log($form);
            console.log($form.serialize());
            $.ajax({
                type: $form.attr('method'),
                url: $form.attr('action'),
                data: $form.serialize(),

                success: function(data, status) {
                    if(data.status && data.status){
                        modal.modal('hide');
                    } else {
                        modal.find(".alert").show();
                        modal.find(".alert .body").show().html("error");

                        if(data.error.error-message){
                            errorBody.html(data.error.error-message);
                        }else {
                            errorBody.html("error");
                        }
                    }
                },
                error: function (request, status, error) {
                    modal.find(".alert").show();
                    modal.find(".alert .body").html(error);
                }
            });
            event.preventDefault();
        });
    }
}

var autocomplete = {
    init: function(){
        $( "#autocomplete" ).autocomplete({
            source: function( request, response ) {
                $.ajax({
                    url: "/api/autocomplete",
                    data: {
                        s: request.term
                    },
                    success: function( data ) {
                        response( data.content );
                    }
                });
            },
            minLength: 3,
            select: function( event, ui ) {
                window.location = '/patients/view/' + ui.item.value;
                return false;
            },
            open: function(event, ui) {
                $(".ui-autocomplete").css("z-index", 1000);
            }
        });
    }
}

/* --------- */

$( document ).ready(function (){
    messageLimit.init("#MessageBody", "#CharactersCounter", 255);
    messageLimit.init("#PanicPushBody", "#PanicPushCharactersCounter", 100);
    messageDate.init();
    time.utcToLocal("utcdate");
    autocomplete.init();
    modal.init("#panicModal");
} );