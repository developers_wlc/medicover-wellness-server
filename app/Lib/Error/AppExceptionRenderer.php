<?php
App::uses('ExceptionRenderer', 'Error');

class AppExceptionRenderer extends ExceptionRenderer {
    protected function _outputMessage($template) {

        if($this->controller->request->params["controller"] == "api"
            || ($this->controller->request->params["controller"] == "patients" && $this->controller->request->params["action"] == "push")
        ){
            $this->controller->layout = null;
            $this->controller->response->type("json");
            parent::_outputMessage("json");
        }else {
            parent::_outputMessage($template);
        }
    }

}