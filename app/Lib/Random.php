<?php

class Random {

    public static function uid($length = 8) {
        return substr(md5(uniqid(mt_rand(), true)), 0, $length);
    }
}