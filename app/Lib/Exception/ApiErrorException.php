<?php

class ApiErrorException extends CakeException {
    public function __construct($message, $code = 0) {
        parent::__construct($message, $code);
        return $this;
    }
}
?>